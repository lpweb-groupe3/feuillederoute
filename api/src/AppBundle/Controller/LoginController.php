<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;

use AppBundle\Entity\Administrateur;
use AppBundle\Entity\Titulaire;
use AppBundle\Entity\Vacataire;

class LoginController extends Controller
{
    /**
     * @Route("/connect")
     */
    public function connectAction(Request $request)
    {
      //object créer pour la redirection des logs
      $logger = $this->get('logger');

      //on récupère le json de la requete
      $datas = json_decode($request->getContent(), true);
      $retJson = array();
      $repository = $this->getDoctrine()->getRepository(Titulaire::class);
      $ret = $repository->findBy([
          "mnemonique" => $datas['mnemonique'],
          //"motDePasse" => md5($datas['motDePasse'])
      ]);
      foreach ($ret as $key ) {
        $infoRet = array(
            'mnemonique' => $key->getMnemonique(),
            'nom' => $key->getNom(),
            'prenom' => $key->getPrenom(),
            'mail' => $key->getMail(),
            'nombreHeureMax' => $key->getNombreHeureMax(),
            'nombreHeureMin' => $key->getNombreHeureMin()
        );
        array_push($retJson, $infoRet);
      }
      //Si titulaire ne renvoie rien on vérifie si il y a dans Vacataire
      if($retJson == null || empty($retJson)){
        $repository = $this->getDoctrine()->getRepository(Vacataire::class);
        $ret = $repository->findBy([
            "mnemonique" => $datas['mnemonique'],
            //"motDePasse" => md5($datas['motDePasse'])
        ]);
        foreach ($ret as $key ) {
          $infoRet = array(
              'mnemonique' => $key->getMnemonique(),
              'nom' => $key->getNom(),
              'prenom' => $key->getPrenom(),
              'mail' => $key->getMail(),
              'nombreHeureMax' => $key->getNombreHeureMax()
          );
          array_push($retJson, $infoRet);
        }
        //Sinon si la recherche vacataire ne donne rien on vérifie Administrateur
        if($ret == null || empty($ret)){
          $repository = $this->getDoctrine()->getRepository(Administrateur::class);
          $ret = $repository->findBy([
              "mnemonique" => $datas['mnemonique'],
              "motDePasse" => md5($datas['motDePasse'])
          ]);
          foreach ($ret as $key ) {
            $infoRet = array(
                'mnemonique' => $key->getMnemonique(),
                'nom' => $key->getNom(),
                'prenom' => $key->getPrenom(),
                'mail' => $key->getMail()
            );
            array_push($retJson, $infoRet);
          }
        }
      }
      /*return new JsonResponse(
          $retJson
      );*/
      $response = new Response();
      $response->setContent(json_encode(
        $retJson
      ));
      $response->headers->set('Content-Type', 'application/json');
      $response->headers->set('Access-Control-Allow-Headers', '*');
      $response->headers->set('Access-Control-Allow-Origin', '*');
      return $response;
    }

    /**
     * @Route("/subscribe")
     */
    public function subscribeAction(Request $request)
    {
      //object créer pour la redirection des logs
      $logger = $this->get('logger');

      //on récupère le json de la requete
      $datas = json_decode($request->getContent(), true);
      $logger->info("tab".print_r($datas,true));

      $em = $this->container->get('doctrine')->getEntityManager();
      $titulaire = "";
      $message = "";

      //si on soumet un formulaire pour un enseignant titulaire
      if($datas['type'] == 'TITULAIRE'){
        $titulaire = new Titulaire();
        $titulaire->setNom($datas['nom']);
        $titulaire->setPrenom($datas['prenom']);
        $titulaire->setMail($datas['mail']);
        $titulaire->setMotDePasse(md5($datas['motDePasse']));
        $titulaire->setNombreHeureMax($datas['nombreHeureMax']);
        $titulaire->setNombreHeureMin($datas['nombreHeureMin']);
        $titulaire->setMnemonique($datas['mnemonique']);
        $em->persist($titulaire);
        $em->flush();
        $message = 'Insertion titulaire réussie';
      }

      //si on soumet un formulaire pour un enseignant vacataire
      else if($datas['type'] == 'VACATAIRE'){
        $vacataire = new Vacataire();
        $vacataire->setNom($datas['nom']);
        $vacataire->setPrenom($datas['prenom']);
        $vacataire->setMail($datas['mail']);
        $vacataire->setMotDePasse(md5($datas['motDePasse']));
        $vacataire->setNombreHeureMax($datas['nombreHeureMax']);
        $vacataire->setMnemonique($datas['mnemonique']);
        $em->persist($vacataire);
        $em->flush();
        $message = 'Insertion Vacataire réussie';
      }

      //si on soumet un formulaire pour un administrateur
      else if($datas['type'] == 'ADMIN'){
        $administrateur = new Administrateur();
        $administrateur->setNom($datas['nom']);
        $administrateur->setPrenom($datas['prenom']);
        $administrateur->setMail($datas['mail']);
        $administrateur->setMotDePasse(md5($datas['motDePasse']));
        $administrateur->setMnemonique($datas['mnemonique']);
        $em->persist($administrateur);
        $em->flush();
        $message = 'Insertion d\'un administrateur réussie';
      }

      //On renvoie le message
      return new JsonResponse(
          $message
      );
    }

    /**
     * @Route("/editUser")
     */
    public function editUserAction(Request $request)
    {
    /*  $datas = json_decode($request->getContent(), true);

      $em = $this->getDoctrine()->getManager();
      $user = $em->getRepository(Titulaire::class)->findBy("mnemonique" => $datas['mnemonique']);
      if (!$user) {
        throw $this->createNotFoundException(
          'No product found for id '.$productId
        );
      }
      $user->setNom($datas['nom']);
      $user->setPrenom($datas['prenom']);
      $user->setMail($datas['mail']);
      $user->setMotDePasse($datas['motDePasse']);

      $em->flush();*/
    }

    //Fonction utilisée pour récupérer un objet à partir du mnémonique
    public function getEntity($mnemonique){
      //object créer pour la redirection des logs
      // $logger = $this->get('logger');
      //on récupère le json de la requete
      $repository = $this->getDoctrine()->getRepository(Titulaire::class);
      $ret = $repository->findBy([
          "mnemonique" => $mnemonique
      ]);
      foreach ($ret as $key ) {
        $obj = $key;
      }
      // print_r($ret,true);
      //Si titulaire ne renvoie rien on vérifie si il y a dans Vacataire
      if($obj == null){
        $repository = $this->getDoctrine()->getRepository(Vacataire::class);
        $ret = $repository->findBy([
            "mnemonique" => $mnemonique
        ]);
        foreach ($ret as $key ) {
          $obj = $key;
        }
        //Sinon si la recherche vacataire ne donne rien on vérifie Administrateur
        if($obj == null){
          $repository = $this->getDoctrine()->getRepository(Administrateur::class);
          $ret = $repository->findBy([
              "mnemonique" => $mnemonique
          ]);
          foreach ($ret as $key ) {
            $obj = $key;
          }
        }
      }
      return $obj;
    }

    /**
     * @Route("/deleteUser")
     */
    public function deleteUserAction(Request $request)
    {
        //on récupère le json de la requete
        $datas = json_decode($request->getContent(), true);
        $user = $this->getEntity($datas['mnemonique']);
        $em = $this->container->get('doctrine')->getEntityManager();
        $em->remove($user);
        $em->flush($user);
        return new JsonResponse("Utilisateur supprimé avec succès".$user->getNom().$user->getMnemonique());
        var_dump($user);
    }

    /**
     * @Route("/showUser")
     */
    public function showAllAction()
    {
      $repository = $this->getDoctrine()->getRepository(Titulaire::class);
      $titulaires = $repository->findAll();
      $repository = $this->getDoctrine()->getRepository(Vacataire::class);
      $vacataires = $repository->findAll();
      $repository = $this->getDoctrine()->getRepository(Administrateur::class);
      $administrateurs = $repository->findAll();

      $titulairesJson = array();
      $vacatairesJson = array();
      $administrateursJson = array();

      foreach ($titulaires as $titulaire){
          $infosTitulaire = array(
              'mnemonique' => $titulaire->getMnemonique(),
              'nom' => $titulaire->getNom(),
              'prenom' => $titulaire->getPrenom(),
              'mail' => $titulaire->getMail(),
              'nombreHeureMax' => $titulaire->getNombreHeureMax(),
              'nombreHeureMin' => $titulaire->getNombreHeureMin()
          );

          array_push($titulairesJson, $infosTitulaire);
      }

      foreach ($vacataires as $vacataire){
          $infosVacataire = array(
              'mnemonique' => $vacataire->getMnemonique(),
              'nom' => $vacataire->getNom(),
              'prenom' => $vacataire->getPrenom(),
              'mail' => $vacataire->getMail(),
              'nombreHeureMax' => $vacataire->getNombreHeureMax()
          );

          array_push($vacatairesJson, $infosVacataire);
      }

      foreach ($administrateurs as $administrateur){
          $infosAdministrateur = array(
              'mnemonique' => $administrateur->getMnemonique(),
              'nom' => $administrateur->getNom(),
              'prenom' => $administrateur->getPrenom(),
              'mail' => $administrateur->getMail()
          );
          array_push($administrateursJson, $infosAdministrateur);
      }
      $utilisateurs = array(
          'titulaires' => $titulairesJson,
          'vacataires' => $vacatairesJson,
          'administrateurs' => $administrateursJson
      );
      return new JsonResponse($utilisateurs);
    }


}
