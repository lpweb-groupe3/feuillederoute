<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Module
 *
 * @ORM\Table(name="module")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ModuleRepository")
 */
class Module
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="string", length=255)
     */
    private $commentaire;

    /**
     * @ORM\ManyToOne(targetEntity=UniteEnseignement::class, inversedBy="modules")
     */
    protected $uniteEnseignement;

    /**
     * @ORM\OneToMany(targetEntity=Enseignement::class, cascade={"persist", "remove"}, mappedBy="module")
     */
    protected $enseignements;

    /**
     * @ORM\ManyToOne(targetEntity=Titulaire::class, inversedBy="modulesResponsable")
     */
    protected $responsable;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Module
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Module
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Module
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return Module
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set uniteEnseignement
     *
     * @param \AppBundle\Entity\UniteEnseignement $uniteEnseignement
     *
     * @return Module
     */
    public function setUniteEnseignement(\AppBundle\Entity\UniteEnseignement $uniteEnseignement = null)
    {
        $this->uniteEnseignement = $uniteEnseignement;

        return $this;
    }

    /**
     * Get uniteEnseignement
     *
     * @return \AppBundle\Entity\UniteEnseignement
     */
    public function getUniteEnseignement()
    {
        return $this->uniteEnseignement;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->enseignements = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add enseignement
     *
     * @param \AppBundle\Entity\Enseignement $enseignement
     *
     * @return Module
     */
    public function addEnseignement(\AppBundle\Entity\Enseignement $enseignement)
    {
        $this->enseignements[] = $enseignement;

        return $this;
    }

    /**
     * Remove enseignement
     *
     * @param \AppBundle\Entity\Enseignement $enseignement
     */
    public function removeEnseignement(\AppBundle\Entity\Enseignement $enseignement)
    {
        $this->enseignements->removeElement($enseignement);
    }

    /**
     * Get enseignements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnseignements()
    {
        return $this->enseignements;
    }

    /**
     * Set responsable
     *
     * @param \AppBundle\Entity\Titulaire $responsable
     *
     * @return Module
     */
    public function setResponsable(\AppBundle\Entity\Titulaire $responsable = null)
    {
        $this->responsable = $responsable;

        return $this;
    }

    /**
     * Get responsable
     *
     * @return \AppBundle\Entity\Titulaire
     */
    public function getResponsable()
    {
        return $this->responsable;
    }
}
