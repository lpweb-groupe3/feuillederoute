<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Vacataire
 *
 * @ORM\Table(name="vacataire")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VacataireRepository")
 */
class Vacataire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotNull
     * @ORM\Column(name="mnemonique", type="string", length=5, unique=true)
     */
    private $mnemonique;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="mail", type="string", length=255)
     */
    private $mail;

    /**
     * @var string
     *
     * @ORM\Column(name="motDePasse", type="string", length=255)
     */
    private $motDePasse;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="string", length=255)
     */
    private $avatar;

    /**
     * @var int
     *
     * @ORM\Column(name="nombreHeureMax", type="smallint")
     */
    private $nombreHeureMax;

    /**
     * @ORM\OneToMany(targetEntity=Association::class, cascade={"persist", "remove"}, mappedBy="vacataire")
     */
    protected $associations;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Vacataire
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Vacataire
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set mail
     *
     * @param string $mail
     *
     * @return Vacataire
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set motDePasse
     *
     * @param string $motDePasse
     *
     * @return Vacataire
     */
    public function setMotDePasse($motDePasse)
    {
        $this->motDePasse = $motDePasse;

        return $this;
    }

    /**
     * Get motDePasse
     *
     * @return string
     */
    public function getMotDePasse()
    {
        return $this->motDePasse;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return Vacataire
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set nombreHeureMax
     *
     * @param integer $nombreHeureMax
     *
     * @return Vacataire
     */
    public function setNombreHeureMax($nombreHeureMax)
    {
        $this->nombreHeureMax = $nombreHeureMax;

        return $this;
    }

    /**
     * Get nombreHeureMax
     *
     * @return int
     */
    public function getNombreHeureMax()
    {
        return $this->nombreHeureMax;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->associations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add association
     *
     * @param \AppBundle\Entity\Association $association
     *
     * @return Vacataire
     */
    public function addAssociation(\AppBundle\Entity\Association $association)
    {
        $this->associations[] = $association;

        return $this;
    }

    /**
     * Remove association
     *
     * @param \AppBundle\Entity\Association $association
     */
    public function removeAssociation(\AppBundle\Entity\Association $association)
    {
        $this->associations->removeElement($association);
    }

    /**
     * Get associations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssociations()
    {
        return $this->associations;
    }

    /**
     * Set mnemonique
     *
     * @param string $mnemonique
     *
     * @return Vacataire
     */
    public function setMnemonique($mnemonique)
    {
        $this->mnemonique = $mnemonique;

        return $this;
    }

    /**
     * Get mnemonique
     *
     * @return string
     */
    public function getMnemonique()
    {
        return $this->mnemonique;
    }
}
