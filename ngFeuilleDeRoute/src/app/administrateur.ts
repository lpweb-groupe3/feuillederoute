export class administrateur {
    nom: string;
    prenom: string;
    mail: string;
    mnemonique: string;

    constructor(nom: string, prenom: string, mail: string, mnemonique: string) {
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.mnemonique = mnemonique;
    }
}