export class Connexion {
    username: string;
    password: string;
    nom: string;
    prenom: string;
    mail: string;
    nbHeureMax: number;
    nbHeureMin: number;
    type: string;
    token: string;
}
