<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoginControllerTest extends WebTestCase
{
    public function testConnect()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');
    }

    public function testSubscribe()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/subscribe');
    }

    public function testEdituser()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/editUser');
    }

    public function testDeleteuser()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/deleteUser');
    }

}
