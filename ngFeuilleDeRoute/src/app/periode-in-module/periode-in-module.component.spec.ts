import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeriodeInModuleComponent } from './periode-in-module.component';

describe('PeriodeInModuleComponent', () => {
  let component: PeriodeInModuleComponent;
  let fixture: ComponentFixture<PeriodeInModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeriodeInModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeriodeInModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
