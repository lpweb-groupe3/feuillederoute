import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { observable } from 'rxjs';
import { Connexion } from '../connexion';
import { Router, ActivatedRoute } from "@angular/router";
import { CookieService } from 'angular2-cookie';
import { titulaire } from '../titulaire';
import { vacataire } from '../vacataire';
import { administrateur } from '../administrateur';
import { sso } from '../sso.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  userList: any = {};
  model: { username: string, password: string };

  constructor(private router: Router, private userservice: UserService, private _cookie: CookieService, private userObject: Connexion, private sso: sso, private routeid: ActivatedRoute) {
    this.model = { username: "", password: "" };
  }

  getCookie() {
    console.log(this._cookie);
  }

  setCookie(key, value) {
    this._cookie.put(key, value);
  }

  submitsimple() {
    let user: any;
    let json = JSON.stringify(this.userList);
    console.log("this.userList" + json);
    //obtenir horaires d'heures min et max du professeur pour savoir dans quel objet le mettre
    if (typeof this.userList[0].nbHeureMax === 'undefined' && typeof this.userList[0].nbHeureMin === 'undefined') {
      user = new administrateur(this.userList[0].nom, this.userList[0].prenom, this.userList[0].mail, this.userList[0].mnemonique);
    } else if (typeof this.userList[0].nbHeureMax === 'undefined' && typeof this.userList[0].nbHeureMin !== 'undefined') {
      user = new vacataire(this.userList[0].nom, this.userList[0].prenom, this.userList[0].mail, this.userList[0].mnemonique, this.userList[0].nbHeureMax);
    } else {
      user = new titulaire(this.userList[0].nom, this.userList[0].prenom, this.userList[0].mail, this.userList[0].mnemonique, this.userList[0].nbHeureMax, this.userList[0].nbHeureMin);
    }
    console.log(user);
    this.setCookie("user", user);
    this.router.navigate(['/navbar']);

    /* var connecte = false;
     this.userList.forEach(element => {
       console.log(element);
       if (element.surname == this.model.username) {
         console.log('Connecté : ' + element.surname);
         connecte = true;
       }
     });
     if (connecte) {
       this.setCookie('username', this.model.username);
       this.router.navigate(['/navbar']);
       console.log(this.userObject.username);
     } else {
       this.userObject = {
         'username': this.model.username,
         'password': this.model.password,
         'nom': "z",
         'prenom': "a",
         'mail': "vf",
         'nbHeureMax': 0,
         'nbHeureMin': 0,
         "type": "cd",
         "token": "cs"
       };
       this.setCookie('username', this.model.username);
       this.router.navigate(['/navbar']);
       console.log(this.userObject.username);
     }*/
  }

  ngOnInit() {
    var id = this.routeid.snapshot.params['id'];
    console.log(id);
    var json = this.sso.getsso(id).subscribe((observer) => {
      console.log(observer)
      if(observer["ret"] == "granted"){
        this.router.navigate(['/navbar']);
        return true;
      } else{
        console.log("problème avec le token");
        return false;
      }
    });
  }

  submitted = false;

  onSubmit() { this.submitted = true; }

  connect() {
    this.userservice.getUser(this.model.username, this.model.password).subscribe((observer) => {
      this.userList = observer;
      console.log(observer);
      if (this.userList != null) {
        this.submitsimple();//observer);
      }
    });
  }

}
