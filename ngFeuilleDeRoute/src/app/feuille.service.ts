import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Connexion } from './connexion';
import { Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export class FeuilleService {

    //private url = 'http://corentin.lpweb-lannion.fr:9919';
    var port = "9999";
    private url = "http://" + window.location.hostname + ":" + port + "/";

    headerOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    constructor(private http: HttpClient) { }

    getFeuille(): Observable<any> {
        console.log(this.url + '/feuille/1')
        return this.http.get(this.url + '/feuille/3');
    }

}
