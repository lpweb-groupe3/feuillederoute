import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Connexion } from './connexion';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  //private url = 'http://corentin.lpweb-lannion.fr:8000/connect';
  //private url = 'http://kevin.lpweb-lannion.fr:9999/connect';
  //private url = 'http://corentin.lpweb-lannion.fr:9919/connect';
  let port = "9999";
  private url = "http://" + window.location.hostname + ":" + port + "/";
  //private url = "http://localhost/feuillederoute/api/web/connect";

  headerOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient, private connect: Connexion) { }

  getAllUsers(): Observable<Connexion> {
    //return this.http.get(this.url);
    console.log(this.url);
    console.log(this.http.get<Connexion>(this.url));
    return this.http.get<Connexion>(this.url);
  }

  updateUser(connect): Observable<Connexion> {
    console.log(JSON.stringify(connect));
    return this.http.put<Connexion>(this.url, JSON.stringify(connect), this.headerOptions);
  }

  getUser(login, pwd): Observable<Connexion> {
    let json = JSON.stringify({ "mnemonique": login, "motDePasse": pwd })
    console.log(json);
    console.log(this.url);
    return this.http.post<Connexion>(this.url, json, this.headerOptions);
  }

  createUser(username, password, nom, prenom, mail, nbHeureMax, nbHeureMin, type): Observable<Connexion> {
    console.log('Creation User');
    let json = JSON.stringify({
      "username": username,
      "password": password,
      "nom": nom,
      "prenom": prenom,
      "mail": mail,
      "nbHeureMax": nbHeureMax,
      "nbHeureMin": nbHeureMin,
      "type": type
    });

    console.log(json);
    return this.http.post<Connexion>(this.url, json, this.headerOptions);
  }

}
