<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MatiereController extends Controller
{
    public function createAction()
    {
        return $this->render('AppBundle:Matiere:create.html.twig', array(
            // ...
        ));
    }

    public function showAction($id)
    {
        return $this->render('AppBundle:Matiere:show.html.twig', array(
            // ...
        ));
    }

    public function showAllAction()
    {
        return $this->render('AppBundle:Matiere:show_all.html.twig', array(
            // ...
        ));
    }

    public function editAction($id)
    {
        return $this->render('AppBundle:Matiere:edit.html.twig', array(
            // ...
        ));
    }

    public function deleteAction($id)
    {
        return $this->render('AppBundle:Matiere:delete.html.twig', array(
            // ...
        ));
    }

}
