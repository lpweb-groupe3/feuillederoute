import { Component, OnInit } from '@angular/core';
import { FeuilleService } from '../feuille.service';
import { getLocaleDayPeriods } from '@angular/common';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-feuille-de-route',
  templateUrl: './feuille-de-route.component.html',
  styleUrls: ['./feuille-de-route.component.css']
})
export class FeuilleDeRouteComponent implements OnInit {

  formation: String = "";
 
  feuille: any = {};
  listeUE: Array<any> = [];

  tabIndexUe: Array<any> = [];
  tabIndexModule: Array<any> = [];
  tabIndexPariode: Array<any> = [];

  listeModuleByUE: Array<any> = [];
  listePeriodeByModule: Array<any> = [];
  listePeriode: Array<any> = [];

  listeHeureCM: Array<any> = [];
  listeHeureTD: Array<any> = [];
  listeHeureTP: Array<any> = [];

  listeGroupe: Array<any> = [];

  listeEnseignementCM: Array<any> = [];

  constructor(private feuilleservice: FeuilleService) { }

  editField: string;

  /*updateList(id: number, property: string, event: any) {
    const editField = event.target.textContent;
    this.personList[id][property] = editField;
  }*/


  changeValue(id: number, property: string, event: any) {
    this.editField = event.target.textContent;
  }

  verifyIfExistInTable(tab, val) {
    let ret = true;
    tab.forEach(element => {
      if (element == val) {
        ret = false;
      }
    });
    return ret;
  }

  getNbUe() {
    return this.listeUE.length;
  }

  getNbModuleByUE(ue) {
    let ret = -1;
    this.listeUE.forEach(element => {
      //console.log(element);
      if (ue == element.idUe) {
        ret = element.modules.length;
      }

    })
    return ret;
  }

  //retourne les periodes
  getPeriodeByModule(id) {
    let ret = [];
    this.listeModuleByUE.forEach(element => {
      if (element.idModule == id) {
        if (element.enseignements != null) {
          // console.log('elt' + element.enseignements);
          element.enseignements.forEach(element => {
            //console.log(element);
            if (element.infosPeriode != null) {
              if (this.verifyIfExistInTable(ret, element.infosPeriode.libelle)) {
                ret.push(element.infosPeriode.libelle);

              } else {
                //console.log('la valeur existe deja dans le tableau a retourner')
              }
            }
          });
        }
      }
    });
    return ret;
  }

  getNbPeriodeByModule(id) {
    //console.log(this.getPeriodeByModule(id).length);
    return this.getPeriodeByModule(id).length;
  }

  getModuleById(idModule) {
    let ret = false;
    this.listeModuleByUE.forEach(element => {
      if (element.idModule == idModule) {
        ret = true;
      }
    })
    return ret;
  }

  //retourne les periodes
  getEnseignementByModule(id) {
    let ret = [];
    this.listeModuleByUE.forEach(element => {
      if (element.idModule == id) {
        if (element.enseignements != null) {
          // console.log('elt' + element.enseignements);
          element.enseignements.forEach(element => {
            //console.log(element);
            if (this.verifyIfExistInTable(ret, element)) {
              ret.push(element);
            }
          });
        }
      }
    });
    return ret;
  }


  getNbCoursByModuleAndPeriode(id_module, periode, type_cours) {
    let ret = 0;
    this.listeModuleByUE.forEach(element => {
      if (element.idModule == id_module) {
        if (element.enseignements != null) {
          element.enseignements.forEach(element => {
            //console.log(element);
            if (element.infosPeriode != null) {
              if (element.infosPeriode.libelle == periode) {
                if (element.typeEnseignement == type_cours) {
                  ret = ret + parseInt(element.associations[0].nbHeures);
                  //console.log('on retourne' + element.associations.nbHeures);
                }
              } else {
                //console.log('la valeur existe deja dans le tableau a retourner')
              }
            }
          });
        }
      }
    });
    //console.log(ret);
    return ret;
  }

  convertGroupToTypeCours(group) {
    if (group == "Promotion") {
      return "CM";
    }
    if (group.length == 1) {
      return "TD";
    }
    if (group.length == 2) {
      return "TP";
    }
  }

  HasEnseignement(typeEnseignement, idModule) {
    let ret = false;
    this.listeModuleByUE.forEach(element => {
      if (element.idModule == idModule) {
        if (element.enseignements.length > 0) {
          element.enseignements.forEach(element => {
            if (element.typeEnseignement == typeEnseignement) {
              ret = true;
            }
          });
        }
      }
    });
    return ret;
  }

  getNbCours(id_module, periode, type_cours) {
    let ret = 0;
    this.listeModuleByUE.forEach(element => {
      if (element.idModule == id_module) {
        if (element.enseignements != null) {
          element.enseignements.forEach(element => {
            //console.log(element);
            if (element.infosPeriode != null) {
              if (element.infosPeriode.libelle == periode) {
                if (element.typeEnseignement == type_cours) {
                  //console.log(element)
                  if (element.associations.length > 0)
                    ret = parseInt(element.associations[0].nbHeures);
                  //console.log('on retourne' + element.associations.nbHeures);
                }
              } else {
                //console.log('la valeur existe deja dans le tableau a retourner')
              }
            }
          });
        }
      }
    });
    //console.log(ret);
    return ret;
  }

  ngOnInit() {
    this.feuilleservice.getFeuille().subscribe((observer) => {
      this.feuille = observer;
      //console.log(this.listeModuleByUE);
      //console.log(observer);
      console.log(observer[1]);

      //on remplit les UE
      observer[1].forEach(element => {
        this.listeUE.push(element);
        let idUe = element.idUe;
        element.modules.forEach(element => {
          this.listeModuleByUE.push(element);
        })

      });

      //on remplit la table des index UE
      let j = 0;
      this.listeUE.forEach(element => {
        //console.log(element.modules);
        this.tabIndexUe[j] = element;
        j++;
      });

      //console.log(this.getPeriodeByModule(4));
      this.listeModuleByUE.forEach(element => {
        //console.log(element.idModule);
        this.listePeriodeByModule.push(this.getPeriodeByModule(element.idModule));
      })

      //on remplit la liste des périodes
      this.listePeriodeByModule.forEach((element) => {
        if (element != null && element != "") {
          element.forEach(element => {
            if (element != null && element != "") {
              this.listePeriode.push(element);
            }
          });
        } else {
          this.listePeriode.push("");
        }
      })

      let i = 0;
      this.listeModuleByUE.forEach(element => {
        this.tabIndexModule[i] = element;
        i++;
      })

      console.log(this.tabIndexModule);
      //on récupère toutes les nombres de cours de chaque Periode de chanque module
      this.tabIndexModule.forEach(element => {
        let idModule = element.idModule;
        if (this.getPeriodeByModule(element.idModule).length != 0
          && this.getPeriodeByModule(element.idModule) != null) {
          this.getPeriodeByModule(element.idModule).forEach(element => {
            let nbCM = this.getNbCours(idModule, element, "CM");
            let nbTD = this.getNbCours(idModule, element, "TD");
            let nbTP = this.getNbCours(idModule, element, "TP");
            //console.log(nbCours);
            if (nbCM != null || nbCM != 0) {
              this.listeHeureCM.push(nbCM);
            }

            if (nbTD != null || nbTD != 0) {
              this.listeHeureTD.push(nbTD);
            }

            if (nbTP != null || nbTP != 0) {
              this.listeHeureTP.push(nbTP);
            }
          })
        } else {
          this.listeHeureCM.push(0);
          this.listeHeureTD.push(0);
          this.listeHeureTP.push(0);
        }
      })

      this.listeModuleByUE.forEach(element => {
        element.enseignements.forEach(element => {

          //console.log(element.associations);
          element.associations.forEach(element => {
            //console.log(element.infosGroupe[0].nom);
            if (element.infosGroupe[0] != null) {
              if (element.infosGroupe[0].nom != null && element.infosGroupe[0].nom != "") {
                //console.log(element.infosGroupe[0].nom)
                if (this.verifyIfExistInTable(this.listeGroupe, element.infosGroupe[0].nom) == true) {
                  this.listeGroupe.push(element.infosGroupe[0].nom);
                }
              } else {
                console.log('nom null');
              }
            }

          });
        });
      })

      /*
            this.tabIndexModule.forEach(element => {
              let idModule = element.idModule;
              if (this.getEnseignementByModule(element.idModule).length != 0
                && this.getEnseignementByModule(element.idModule) != null) {
                this.getEnseignementByModule(element.idModule).forEach(element => {
                  //console.log(element)
                  if (element.typeEnseignement == "CM") {
                    console.log(element)
                  } else {
                    if (element.typeEnseignement != "TP" && element.typeEnseignement != "TD") {
                      //console.log('no');
                    }
      
      
                  }
                })
              } else {
                //console.log('vide');
              }
            })*/

      //console.log(tabCM);
      /*console.log(this.listeModuleByUE);  
      console.log(this.getEnseignementByModuleAndPeriode(7, "P3", "CM", "Promotion"));*/
      //console.log(this.getEnseignementByModuleAndPeriode(5, "P2", "TP", "G2"));

    });
  }

  /**
   * Retourne un tableau contenant la ligne pour HTML 
   * des enseignements d'un groupe
   * @param group 
   */
  getEnseignement(group) {
    //console.log(group);
    let type_group = this.convertGroupToTypeCours(group);
    let tab = [];
    this.tabIndexModule.forEach(element => {
      let idMod = element.idModule;
      if (this.getPeriodeByModule(element.idModule).length != 0
        && this.getPeriodeByModule(element.idModule) != null) {
        this.getPeriodeByModule(element.idModule).forEach(element => {
          tab.push(this.getEnseignementByModuleAndPeriode(idMod, element, type_group, group));
        })
      } else {
        tab.push([]);
        //console.log('vide');
      }
    })
    return tab;
  }

  /**
   * Récupère le mnemonique de l'enseignant pour un Module une période et un groupe
   * @param idModule 
   * @param periode 
   * @param type_cours 
   * @param groupe 
   */
  getEnseignementByModuleAndPeriode(idModule, periode, type_cours, groupe) {
    let ret = [];
    //console.log('debut');
    this.listeModuleByUE.forEach(element => {
      if (element.idModule == idModule) {
        element.enseignements.forEach(element => {
          let elt = element;
          if (element.typeEnseignement == type_cours
            && element.infosPeriode.libelle == periode) {
            //console.log(elt)
            element.associations.forEach(element => {
              console.log(element);
              if (element.infosGroupe.length > 0) {
                if (element.infosGroupe[0].nom == groupe) {
                  //console.log(element);
                  ret.push(element.infosProf.mnemonique);
                }
              } else {
                //ret.push('');
              }

            });

          }
        });
      }
    });
    return ret;
  }
}