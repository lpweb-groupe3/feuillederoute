<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Titulaire
 *
 * @ORM\Table(name="titulaire")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TitulaireRepository")
 */
class Titulaire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotNull
     * @ORM\Column(name="mnemonique", type="string", length=5, unique=true)
     */
    private $mnemonique;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="mail", type="string", length=255)
     */
    private $mail;

    /**
     * @var string
     *
     * @ORM\Column(name="motDePasse", type="string", length=255)
     */
    private $motDePasse;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="string", length=255)
     */
    private $avatar;

    /**
     * @var int
     *
     * @ORM\Column(name="nombreHeureMax", type="smallint")
     */
    private $nombreHeureMax;

    /**
     * @var int
     *
     * @ORM\Column(name="nombreHeureMin", type="smallint")
     */
    private $nombreHeureMin;

    /**
     * @ORM\OneToMany(targetEntity=Module::class, cascade={"persist", "remove"}, mappedBy="responsable")
     */
    protected $modulesResponsable;

    /**
     * @ORM\OneToMany(targetEntity=Association::class, cascade={"persist", "remove"}, mappedBy="titulaire")
     */
    protected $associations;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Titulaire
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Titulaire
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set mail
     *
     * @param string $mail
     *
     * @return Titulaire
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set motDePasse
     *
     * @param string $motDePasse
     *
     * @return Titulaire
     */
    public function setMotDePasse($motDePasse)
    {
        $this->motDePasse = $motDePasse;

        return $this;
    }

    /**
     * Get motDePasse
     *
     * @return string
     */
    public function getMotDePasse()
    {
        return $this->motDePasse;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return Titulaire
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set nombreHeureMax
     *
     * @param integer $nombreHeureMax
     *
     * @return Titulaire
     */
    public function setNombreHeureMax($nombreHeureMax)
    {
        $this->nombreHeureMax = $nombreHeureMax;

        return $this;
    }

    /**
     * Get nombreHeureMax
     *
     * @return int
     */
    public function getNombreHeureMax()
    {
        return $this->nombreHeureMax;
    }

    /**
     * Set nombreHeureMin
     *
     * @param integer $nombreHeureMin
     *
     * @return Titulaire
     */
    public function setNombreHeureMin($nombreHeureMin)
    {
        $this->nombreHeureMin = $nombreHeureMin;

        return $this;
    }

    /**
     * Get nombreHeureMin
     *
     * @return int
     */
    public function getNombreHeureMin()
    {
        return $this->nombreHeureMin;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->modulesResponsable = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add modulesResponsable
     *
     * @param \AppBundle\Entity\Module $modulesResponsable
     *
     * @return Titulaire
     */
    public function addModulesResponsable(\AppBundle\Entity\Module $modulesResponsable)
    {
        $this->modulesResponsable[] = $modulesResponsable;

        return $this;
    }

    /**
     * Remove modulesResponsable
     *
     * @param \AppBundle\Entity\Module $modulesResponsable
     */
    public function removeModulesResponsable(\AppBundle\Entity\Module $modulesResponsable)
    {
        $this->modulesResponsable->removeElement($modulesResponsable);
    }

    /**
     * Get modulesResponsable
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModulesResponsable()
    {
        return $this->modulesResponsable;
    }

    /**
     * Add association
     *
     * @param \AppBundle\Entity\Association $association
     *
     * @return Titulaire
     */
    public function addAssociation(\AppBundle\Entity\Association $association)
    {
        $this->associations[] = $association;

        return $this;
    }

    /**
     * Remove association
     *
     * @param \AppBundle\Entity\Association $association
     */
    public function removeAssociation(\AppBundle\Entity\Association $association)
    {
        $this->associations->removeElement($association);
    }

    /**
     * Get associations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssociations()
    {
        return $this->associations;
    }

    /**
     * Set mnemonique
     *
     * @param string $mnemonique
     *
     * @return Titulaire
     */
    public function setMnemonique($mnemonique)
    {
        $this->mnemonique = $mnemonique;

        return $this;
    }

    /**
     * Get mnemonique
     *
     * @return string
     */
    public function getMnemonique()
    {
        return $this->mnemonique;
    }
}
