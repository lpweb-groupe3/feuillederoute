import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-affectation',
  templateUrl: './affectation.component.html',
  styleUrls: ['./affectation.component.css']
})
export class AffectationComponent implements OnInit {

  constructor(private userservice: UserService) { }

  ngOnInit() {
    let json = this.userservice.getAllUsers();
    console.log(json);
  }

  options = [1, 2, 3];
  optionSelected: any;

  onOptionSelected() {
    console.log(this.optionSelected); //option value will be sent as event
  }

}
