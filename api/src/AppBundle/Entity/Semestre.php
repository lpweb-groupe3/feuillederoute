<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Semestre
 *
 * @ORM\Table(name="semestre")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SemestreRepository")
 */
class Semestre
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebut", type="datetime")
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFin", type="datetime")
     */
    private $dateFin;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Annee_scolaire::class, inversedBy="semestres")
     */
    protected $anneeScolaire;

    /**
     * @ORM\OneToMany(targetEntity=UniteEnseignement::class, cascade={"persist", "remove"}, mappedBy="semestre")
     */
    protected $unitesEnseignement;

    /**
     * @ORM\ManyToOne(targetEntity=Diplome::class, inversedBy="semestres")
     */
    protected $diplome;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Semestre
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     *
     * @return Semestre
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     *
     * @return Semestre
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Semestre
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set anneeScolaire
     *
     * @param \AppBundle\Entity\Annee_scolaire $anneeScolaire
     *
     * @return Semestre
     */
    public function setAnneeScolaire(\AppBundle\Entity\Annee_scolaire $anneeScolaire = null)
    {
        $this->anneeScolaire = $anneeScolaire;

        return $this;
    }

    /**
     * Get anneeScolaire
     *
     * @return \AppBundle\Entity\Annee_scolaire
     */
    public function getAnneeScolaire()
    {
        return $this->anneeScolaire;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->unitesEnseignement = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add unitesEnseignement
     *
     * @param \AppBundle\Entity\UniteEnseignement $unitesEnseignement
     *
     * @return Semestre
     */
    public function addUnitesEnseignement(\AppBundle\Entity\UniteEnseignement $unitesEnseignement)
    {
        $this->unitesEnseignement[] = $unitesEnseignement;

        return $this;
    }

    /**
     * Remove unitesEnseignement
     *
     * @param \AppBundle\Entity\UniteEnseignement $unitesEnseignement
     */
    public function removeUnitesEnseignement(\AppBundle\Entity\UniteEnseignement $unitesEnseignement)
    {
        $this->unitesEnseignement->removeElement($unitesEnseignement);
    }

    /**
     * Get unitesEnseignement
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUnitesEnseignement()
    {
        return $this->unitesEnseignement;
    }

    /**
     * Set diplome
     *
     * @param \AppBundle\Entity\Diplome $diplome
     *
     * @return Semestre
     */
    public function setDiplome(\AppBundle\Entity\Diplome $diplome = null)
    {
        $this->diplome = $diplome;

        return $this;
    }

    /**
     * Get diplome
     *
     * @return \AppBundle\Entity\Diplome
     */
    public function getDiplome()
    {
        return $this->diplome;
    }
}
