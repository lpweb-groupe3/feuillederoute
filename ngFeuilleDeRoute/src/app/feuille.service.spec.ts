import { TestBed } from '@angular/core/testing';

import { FeuilleService } from './feuille.service';

describe('FeuilleService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: FeuilleService = TestBed.get(FeuilleService);
        expect(service).toBeTruthy();
    });
});
