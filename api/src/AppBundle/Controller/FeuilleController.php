<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class FeuilleController extends Controller
{
    public function createAction()
    {
        return $this->render('AppBundle:Feuille:create.html.twig', array(
            // ...
        ));
    }

    public function showAction($idSemestre)
    {
        $repositorySemestre= $this->getDoctrine()->getManager()->getRepository('AppBundle:Semestre');

        //Si l'identifiant de semestre existe
        if (($semestre = $repositorySemestre->findById($idSemestre)) != null){
            $response = array();

            $repositoryUE= $this->getDoctrine()->getManager()->getRepository('AppBundle:UniteEnseignement');


            $infosAnnee = array(
                'jourDebut' => $semestre[0]->getAnneeScolaire()->getJourDebut(),
                'jourFin' => $semestre[0]->getAnneeScolaire()->getJourFin()
            );

            $infosSemestre = array(
                'nomDiplome' => $semestre[0]->getDiplome()->getNom(),
                'nomDept' => $semestre[0]->getDiplome()->getDepartement()->getNom(),
                'infosAnnee' => $infosAnnee
            );

            array_push($response, $infosSemestre);

            $unitesEns = $repositoryUE->findBySemestre($semestre[0]);

            //Récupération de la liste des UE
            $listeUe = self::getListeUE($unitesEns);

            array_push($response, $listeUe);
        }else{
            $response = array(
              'erreur' => 'L\'identifiant de semestre est incorrect'
            );
        }

        return new JsonResponse($response);
    }

    /**
     * @param $unitesEns liste d'UE
     * @return array $liste qui est un JSON de la liste des UE
     */
    public function getListeUE($unitesEns){
        $liste = array();

        foreach ($unitesEns as $uniteEns){
            $idUe = $uniteEns->getId();

            //Récupération de la liste des modules
            $modules = $uniteEns->getModules();
            $listeModules = self::getListeModules($modules);

            $infosUe = array(
                'idUe' => $idUe,
                'nomUe' => $uniteEns->getNom(),
                'modules' => $listeModules
            );
            array_push($liste, $infosUe);
        }

        return $liste;
    }


    /**
     * @param $modules liste de module
     * @return array $liste qui est un JSON de la liste des modules
     */
    public function getListeModules($modules){
        $liste = array();

        foreach ($modules as $module){
            $idModule = $module->getId();

            $responsable = $module->getResponsable();

            $infosResp = array(
                'mnemonique' => $responsable->getMnemonique(),
                'nom' =>   $responsable->getNom(),
                'prenom' =>   $responsable->getPrenom()
            );

            $infosModule = array(
                'idModule' => $idModule,
                'codeModule' => $module->getCode(),
                'nomModule' => $module->getLibelle(),
                'responsableModule' => $infosResp,
                'enseignements' => self::getEnseignements($module)
            );
            array_push($liste, $infosModule);
        }

        return $liste;
    }

    public function getEnseignements($module){
        $enseignements = $module->getEnseignements();

        $liste = array();

        foreach ($enseignements as $enseignement) {

            $typeEns = '';

            if ($enseignement->getCm()){
                $typeEns = 'CM';
            }
            elseif ($enseignement->getTd()){
                $typeEns = 'TD';
            }
            elseif ($enseignement->getTp()){
                $typeEns = 'TP';
            }

            $periode = $enseignement->getPeriode();

            $infosPeriode = array(
                'libelle' => $periode->getLibelle(),
                'dateDebut' => $periode->getDateDebut(),
                'dateFin' => $periode->getDateFin()
            );



            $infosEnseignement = array(
                'nomEnseignement' => $enseignement->getNom(),
                'typeEnseignement' => $typeEns,
                'infosPeriode' => $infosPeriode,
                'associations' => self::getAssociations($enseignement)
            );
            array_push($liste, $infosEnseignement);
        }

        return $liste;
    }

    public function getAssociations($enseignement){

        $associations = $this->getDoctrine()->getManager()->getRepository('AppBundle:Association')->findByEnseignement($enseignement);

        $liste = array();

        foreach ($associations as $association) {

            $typeProf = '';
            $infosProf = '';

            if ($association->getTitulaire()){
                $prof = $association->getTitulaire();
                $typeProf = 'T';
                $infosProf = array(
                    'nom' => $prof->getNom(),
                    'prenom' => $prof->getPrenom(),
                    'mnemonique' => $prof->getMnemonique(),
                    'nbHeuresMax' => $prof->getNombreHeureMax(),
                    'nbHeuresMin' => $prof->getNombreHeureMin()
                );
            }
            elseif ($association->getVacataire()){
                $prof = $association->getVacataire();
                $typeProf = 'V';
                $infosProf = array(
                    'nom' => $prof->getNom(),
                    'prenom' => $prof->getPrenom(),
                    'mnemonique' => $prof->getMnemonique(),
                    'nbHeuresMax' => $prof->getNombreHeureMax(),
                    'nbHeuresMin' => $prof->getNombreHeureMin()
                );
            }

            $infosGroupe = array();
            $infosGroupes = array();

            if (sizeof($association->getGroupesCM()) > 0){
                $groupes = $association->getGroupesCM();

                foreach ($groupes as $groupe) {
                    $infosGroupe = array(
                        'nom' => $groupe->getNom()
                    );
                    array_push($infosGroupes, $infosGroupe);
                }
            }
            elseif (sizeof($association->getGroupesTD()) > 0){
                $groupes = $association->getGroupesTD();
                foreach ($groupes as $groupe) {
                    $infosGroupe = array(
                        'nom' => $groupe->getNom()
                    );
                    array_push($infosGroupes, $infosGroupe);
                }

            }
            elseif (sizeof($association->getGroupesTP()) > 0){
                $groupes = $association->getGroupesTP();

               foreach ($groupes as $groupe) {
                    $infosGroupe = array(
                        'nom' => $groupe->getNom()
                    );
                    array_push($infosGroupes, $infosGroupe);
                }
            }

            $infosAssociation = array(
                'nbHeures' => $association->getNbHeures(),
                'typeProf' => $typeProf,
                'temporaire' => $association->getTemp(),
                'infosProf' => $infosProf,
                'infosGroupe' => $infosGroupes
            );
            array_push($liste, $infosAssociation);
        }

        return $liste;
    }


    public function editAction($id)
    {
        return $this->render('AppBundle:Feuille:edit.html.twig', array(
            // ...
        ));
    }

    public function deleteAction($id)
    {
        return $this->render('AppBundle:Feuille:delete.html.twig', array(
            // ...
        ));
    }

    public function showAllAction()
    {
        return $this->render('AppBundle:Feuille:show_all.html.twig', array(
            // ...
        ));
    }

    public function getAllDepartementsAction(){
        $repositoryDepartement= $this->getDoctrine()->getManager()->getRepository('AppBundle:Departement');
        $departements = $repositoryDepartement->findAll();

        $listeDepartements = array();

        foreach ($departements as $departement){
            $diplomes = $departement->getDiplomes();
            $listeDiplomes = array();
            foreach ($diplomes as $diplome){
                $semestres = $diplome->getSemestres();
                $listeSemestres = array();
                foreach ($semestres as $semestre){
                    $infosSemestre = array(
                        'idSem' => $semestre->getId(),
                        'nomSem' => $semestre->getNom()
                    );
                    array_push($listeSemestres, $infosSemestre);
                }

                $infosDiplomes = array(
                    'idDip' => $diplome->getId(),
                    'nomDip' => $diplome->getNom(),
                    'semestres' => $listeSemestres
                );
                array_push($listeDiplomes, $infosDiplomes);
            }

            $infosDepartement = array(
                'idDept' => $departement->getId(),
                'nomDept' => $departement->getNom(),
                'diplomes' => $listeDiplomes
            );

            array_push($listeDepartements, $infosDepartement);
        }

        return new JsonResponse($listeDepartements);
    }

}
