<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Enseignement
 *
 * @ORM\Table(name="enseignement")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EnseignementRepository")
 */
class Enseignement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="cm", type="boolean")
     */
    private $cm;

    /**
     * @var bool
     *
     * @ORM\Column(name="tp", type="boolean")
     */
    private $tp;

    /**
     * @var bool
     *
     * @ORM\Column(name="td", type="boolean")
     */
    private $td;

    /**
     * @ORM\ManyToOne(targetEntity=Module::class, inversedBy="enseignements")
     */
    protected $module;

    /**
     * @ORM\ManyToOne(targetEntity=Periode::class, inversedBy="enseignements")
     */
    protected $periode;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Enseignement
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Enseignement
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set cm
     *
     * @param boolean $cm
     *
     * @return Enseignement
     */
    public function setCm($cm)
    {
        $this->cm = $cm;

        return $this;
    }

    /**
     * Get cm
     *
     * @return bool
     */
    public function getCm()
    {
        return $this->cm;
    }

    /**
     * Set tp
     *
     * @param boolean $tp
     *
     * @return Enseignement
     */
    public function setTp($tp)
    {
        $this->tp = $tp;

        return $this;
    }

    /**
     * Get tp
     *
     * @return bool
     */
    public function getTp()
    {
        return $this->tp;
    }

    /**
     * Set td
     *
     * @param boolean $td
     *
     * @return Enseignement
     */
    public function setTd($td)
    {
        $this->td = $td;

        return $this;
    }

    /**
     * Get td
     *
     * @return bool
     */
    public function getTd()
    {
        return $this->td;
    }

    /**
     * Set module
     *
     * @param \AppBundle\Entity\Module $module
     *
     * @return Enseignement
     */
    public function setModule(\AppBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return \AppBundle\Entity\Module
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set periode
     *
     * @param \AppBundle\Entity\Periode $periode
     *
     * @return Enseignement
     */
    public function setPeriode(\AppBundle\Entity\Periode $periode = null)
    {
        $this->periode = $periode;

        return $this;
    }

    /**
     * Get periode
     *
     * @return \AppBundle\Entity\Periode
     */
    public function getPeriode()
    {
        return $this->periode;
    }


}
