<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Diplome
 *
 * @ORM\Table(name="diplome")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DiplomeRepository")
 */
class Diplome
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Groupe_cm::class, cascade={"persist", "remove"}, mappedBy="diplome")
     */
    protected $groupesCM;

    /**
     * @ORM\OneToMany(targetEntity=Groupe_td::class, cascade={"persist", "remove"}, mappedBy="diplome")
     */
    protected $groupesTD;

    /**
     * @ORM\OneToMany(targetEntity=Groupe_tp::class, cascade={"persist", "remove"}, mappedBy="diplome")
     */
    protected $groupesTP;

    /**
     * @ORM\ManyToOne(targetEntity=Departement::class, inversedBy="diplomes")
     */
    protected $departement;

    /**
     * @ORM\OneToMany(targetEntity=Semestre::class, cascade={"persist", "remove"}, mappedBy="diplome")
     */
    protected $semestres;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Diplome
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Diplome
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->groupesCM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->groupesTD = new \Doctrine\Common\Collections\ArrayCollection();
        $this->groupesTP = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add groupesCM
     *
     * @param \AppBundle\Entity\Groupe_cm $groupesCM
     *
     * @return Diplome
     */
    public function addGroupesCM(\AppBundle\Entity\Groupe_cm $groupesCM)
    {
        $this->groupesCM[] = $groupesCM;

        return $this;
    }

    /**
     * Remove groupesCM
     *
     * @param \AppBundle\Entity\Groupe_cm $groupesCM
     */
    public function removeGroupesCM(\AppBundle\Entity\Groupe_cm $groupesCM)
    {
        $this->groupesCM->removeElement($groupesCM);
    }

    /**
     * Get groupesCM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupesCM()
    {
        return $this->groupesCM;
    }

    /**
     * Add groupesTD
     *
     * @param \AppBundle\Entity\Groupe_td $groupesTD
     *
     * @return Diplome
     */
    public function addGroupesTD(\AppBundle\Entity\Groupe_td $groupesTD)
    {
        $this->groupesTD[] = $groupesTD;

        return $this;
    }

    /**
     * Remove groupesTD
     *
     * @param \AppBundle\Entity\Groupe_td $groupesTD
     */
    public function removeGroupesTD(\AppBundle\Entity\Groupe_td $groupesTD)
    {
        $this->groupesTD->removeElement($groupesTD);
    }

    /**
     * Get groupesTD
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupesTD()
    {
        return $this->groupesTD;
    }

    /**
     * Add groupesTP
     *
     * @param \AppBundle\Entity\Groupe_tp $groupesTP
     *
     * @return Diplome
     */
    public function addGroupesTP(\AppBundle\Entity\Groupe_tp $groupesTP)
    {
        $this->groupesTP[] = $groupesTP;

        return $this;
    }

    /**
     * Remove groupesTP
     *
     * @param \AppBundle\Entity\Groupe_tp $groupesTP
     */
    public function removeGroupesTP(\AppBundle\Entity\Groupe_tp $groupesTP)
    {
        $this->groupesTP->removeElement($groupesTP);
    }

    /**
     * Get groupesTP
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupesTP()
    {
        return $this->groupesTP;
    }

    /**
     * Set departement
     *
     * @param \AppBundle\Entity\Departement $departement
     *
     * @return Diplome
     */
    public function setDepartement(\AppBundle\Entity\Departement $departement = null)
    {
        $this->departement = $departement;

        return $this;
    }

    /**
     * Get departement
     *
     * @return \AppBundle\Entity\Departement
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * Add semestre
     *
     * @param \AppBundle\Entity\Semestre $semestre
     *
     * @return Diplome
     */
    public function addSemestre(\AppBundle\Entity\Semestre $semestre)
    {
        $this->semestres[] = $semestre;

        return $this;
    }

    /**
     * Remove semestre
     *
     * @param \AppBundle\Entity\Semestre $semestre
     */
    public function removeSemestre(\AppBundle\Entity\Semestre $semestre)
    {
        $this->semestres->removeElement($semestre);
    }

    /**
     * Get semestres
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSemestres()
    {
        return $this->semestres;
    }
}
