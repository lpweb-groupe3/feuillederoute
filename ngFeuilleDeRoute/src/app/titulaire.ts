
export class titulaire {
    nom: string;
    prenom: string;
    mail: string;
    mnemonique: string;
    nbHeureMax: Number;
    nbHeureMin: Number;

    constructor(nom: string, prenom: string, mail: string, mnemonique: string, nbHeureMax: Number, nbHeureMin: Number) {
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.mnemonique = mnemonique;
        this.nbHeureMax = nbHeureMax;
        this.nbHeureMin = nbHeureMin;
    }
}