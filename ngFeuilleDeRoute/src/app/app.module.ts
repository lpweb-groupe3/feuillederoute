//core elements
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { Connexion } from './connexion';
import { UserService } from './user.service';
/*import { titulaire } from './titulaire';
import { vacataire } from './vacataire';
import { administrateur } from './administrateur';*/
import { sso } from './sso.service';
//roads import
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
//Components
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { AffectationComponent } from './affectation/affectation.component';
import { PeriodeInModuleComponent } from './periode-in-module/periode-in-module.component';
import { FeuilleDeRouteComponent } from './feuille-de-route/feuille-de-route.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AddUserComponent } from './add-user/add-user.component';
import { ModalComponent } from './modal/modal.component';

//Roads
const appRoutes: Routes = [
  { path: 'modal', component: ModalComponent },
  { path: 'feuille', component: FeuilleDeRouteComponent },
  { path: 'connexion', component: ConnexionComponent },
  { path: 'connexion/:id', component: ConnexionComponent },
  { path: 'navbar', component: NavbarComponent },
  { path: 'affectation', component: AffectationComponent },
  { path: 'adduser', component: AddUserComponent },
  { path: '', component: ConnexionComponent },
  { path: '**', component: ConnexionComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ConnexionComponent,
    AffectationComponent,
    PeriodeInModuleComponent,
    FeuilleDeRouteComponent,
    NavbarComponent,
    AddUserComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
    MDBBootstrapModule.forRoot()

  ],
  providers: [CookieService, Connexion, UserService],//, titulaire, vacataire, administrateur],
  bootstrap: [AppComponent]
})
export class AppModule { }
