<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class EnseignantController extends Controller
{
    /**
     * Création d'un enseignant
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $tabTitulaire= $em->getRepository('AppBundle:Titulaire')->findAll();
        $tabVacataire= $em->getRepository('AppBundle:Vacataire')->findAll();

        return new JsonResponse(['cree' => true]);
    }

    /**
     * Pour obtenir les informations d'un enseignant
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($mnemo)
    {
        $repositoryTitulaire = $this->getDoctrine()->getManager()->getRepository('AppBundle:Titulaire');
        $repositoryVacataire = $this->getDoctrine()->getManager()->getRepository('AppBundle:Vacataire');
        $repositoryAssociations = $this->getDoctrine()->getManager()->getRepository('AppBundle:Association');

        $enseignantJson = null;

        if (($enseignant = $repositoryTitulaire->findByMnemonique($mnemo)) != null){
            $enseignant = $enseignant[0];

            $id = $enseignant->getId();

            $associations = $repositoryAssociations->findByTitulaire($id);

            $enseignements = array();

            foreach ($associations as $association){
                $enseignement = $association->getEnseignement();

                $typeEnseignement = '';
                $groupes = null;


                if ($enseignement->getCm() != null){
                    $typeEnseignement = 'CM';
                    $groupesCM = $association->getGroupesCM();

                    $groupes = array();

                    foreach ($groupesCM as $groupeCM){

                        $groupe = array(
                            'nomGroupe' => $groupeCM->getNom(),
                            'diplome' => $groupeCM->getDiplome()->getNom()
                        );
                        array_push($groupes, $groupe);
                    }

                }
                elseif ($enseignement->getTd() != null){
                    $typeEnseignement = 'TD';
                    $groupesTD = $association->getGroupesTD();

                    $groupes = array();

                    foreach ($groupesTD as $groupeTD){

                        $groupe = array(
                            'nomGroupe' => $groupeTD->getNom(),
                            'diplome' => $groupeTD->getDiplome()->getNom()
                        );
                        array_push($groupes, $groupe);
                    }
                }
                elseif ($enseignement->getTp() != null){
                    $typeEnseignement = 'TP';
                    $groupesTP = $association->getGroupesTP();

                    $groupes = array();

                    foreach ($groupesTP as $groupeTP){

                        $groupe = array(
                            'nomGroupe' => $groupeTP->getNom(),
                            'diplome' => $groupeTP->getDiplome()->getNom()
                        );
                        array_push($groupes, $groupe);
                    }
                }


                $infoEnseignement = array(
                    'nomEnseignement' => $enseignement->getNom(),
                    'typeEnseignement' => $typeEnseignement,
                    'periode' => $enseignement->getPeriode()->getLibelle(),
                    'dateDebutPeriode' => $enseignement->getPeriode()->getDateDebut(),
                    'dateFinPeriode' => $enseignement->getPeriode()->getDateFin(),
                    'groupes' => $groupes

                );
                array_push($enseignements, $infoEnseignement);
            }

            $enseignantJson = new JsonResponse([
                'mnemonique' => $enseignant->getMnemonique(),
                'nom' => $enseignant->getNom(),
                'prenom' => $enseignant->getPrenom(),
                'mail' => $enseignant->getMail(),
                'nombreHeureMin' => $enseignant->getNombreHeureMin(),
                'nombreHeureMax' => $enseignant->getNombreHeureMax(),
                'estTitulaire' => true,
                'enseignements' => $enseignements
            ]);

        }
        elseif (($enseignant = $repositoryVacataire->findByMnemonique($mnemo)) != null){
            $enseignant = $enseignant[0];
            $enseignantJson = new JsonResponse([
                'mnemonique' => $enseignant->getMnemonique(),
                'nom' => $enseignant->getNom(),
                'prenom' => $enseignant->getPrenom(),
                'mail' => $enseignant->getMail(),
                'nombreHeureMax' => $enseignant->getNombreHeureMax(),
                'estTitulaire' => false
            ]);
        }
        else {
            $enseignantJson = new JsonResponse();
        }

        return $enseignantJson;
    }

    /**
     * Pour obtenir la liste de tous les enseignants
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAllAction()
    {
        $repositoryTitulaire = $this->getDoctrine()->getManager()->getRepository('AppBundle:Titulaire');
        $repositoryVacataire = $this->getDoctrine()->getManager()->getRepository('AppBundle:Vacataire');


        $titulaires = $repositoryTitulaire->findAll();
        $vacataires = $repositoryVacataire->findAll();

        $titulairesJson = array();
        $vacatairesJson = array();

        foreach ($titulaires as $titulaire){
            $infosTitulaire = array(
                'mnemonique' => $titulaire->getMnemonique(),
                'nom' => $titulaire->getNom(),
                'prenom' => $titulaire->getPrenom(),
                'mail' => $titulaire->getMail(),
                'nombreHeureMax' => $titulaire->getNombreHeureMax(),
                'estTitulaire' => true
            );

            array_push($titulairesJson, $infosTitulaire);
        }

        foreach ($vacataires as $vacataire){
            $infosVacataire = array(
                'mnemonique' => $vacataire->getMnemonique(),
                'nom' => $vacataire->getNom(),
                'prenom' => $vacataire->getPrenom(),
                'mail' => $vacataire->getMail(),
                'nombreHeureMax' => $vacataire->getNombreHeureMax(),
                'estTitulaire' => false
            );

            array_push($vacatairesJson, $infosVacataire);
        }

        $response = new Response(
            'Content',
            Response::HTTP_OK,
            ['content-type' => 'application/json']
        );

        $response->setContent(json_encode([
          'titulaires' => $titulairesJson,
          'vacataires' => $vacatairesJson
        ]));

        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
        // return new JsonResponse($enseignants)->headers->set('Access-Control-Allow-Origin', '*');
    }

    /**
     * Pour obtenir la liste de tous les mnémoniques
     * @return JsonResponse mnemoniques
     */
    public function getMnemoniquesAction(){
        $repositoryTitulaire = $this->getDoctrine()->getManager()->getRepository('AppBundle:Titulaire');
        $repositoryVacataire = $this->getDoctrine()->getManager()->getRepository('AppBundle:Vacataire');

        $titulaires = $repositoryTitulaire->findAll();
        $vacataires = $repositoryVacataire->findAll();

        $mnemoniquesesJson = array();

        foreach ($titulaires as $titulaire){
            array_push($mnemoniquesesJson, $titulaire->getMnemonique());
        }

        foreach ($vacataires as $vacataire){
            array_push($mnemoniquesesJson, $vacataire->getMnemonique());
        }

        return new JsonResponse($mnemoniquesesJson);
    }

    /**
     * Pour modifier un enseignant
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction()
    {
        return $this->render('AppBundle:Enseignant:edit.html.twig', array(
            // ...
        ));
    }

    /**
     * Pour supprimer un enseignant
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction()
    {
        return $this->render('AppBundle:Enseignant:delete.html.twig', array(
            // ...
        ));
    }

}
