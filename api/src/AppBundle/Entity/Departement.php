<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Departement
 *
 * @ORM\Table(name="departement")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DepartementRepository")
 */
class Departement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Diplome::class, cascade={"persist", "remove"}, mappedBy="departement")
     */
    protected $diplomes;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Departement
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Departement
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->groupesCM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->groupesTD = new \Doctrine\Common\Collections\ArrayCollection();
        $this->groupesTP = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add groupesCM
     *
     * @param \AppBundle\Entity\Groupe_cm $groupesCM
     *
     * @return Departement
     */
    public function addGroupesCM(\AppBundle\Entity\Groupe_cm $groupesCM)
    {
        $this->groupesCM[] = $groupesCM;

        return $this;
    }

    /**
     * Remove groupesCM
     *
     * @param \AppBundle\Entity\Groupe_cm $groupesCM
     */
    public function removeGroupesCM(\AppBundle\Entity\Groupe_cm $groupesCM)
    {
        $this->groupesCM->removeElement($groupesCM);
    }

    /**
     * Get groupesCM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupesCM()
    {
        return $this->groupesCM;
    }

    /**
     * Add groupesTD
     *
     * @param \AppBundle\Entity\Groupe_td $groupesTD
     *
     * @return Departement
     */
    public function addGroupesTD(\AppBundle\Entity\Groupe_td $groupesTD)
    {
        $this->groupesTD[] = $groupesTD;

        return $this;
    }

    /**
     * Remove groupesTD
     *
     * @param \AppBundle\Entity\Groupe_td $groupesTD
     */
    public function removeGroupesTD(\AppBundle\Entity\Groupe_td $groupesTD)
    {
        $this->groupesTD->removeElement($groupesTD);
    }

    /**
     * Get groupesTD
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupesTD()
    {
        return $this->groupesTD;
    }

    /**
     * Add groupesTP
     *
     * @param \AppBundle\Entity\Groupe_tp $groupesTP
     *
     * @return Departement
     */
    public function addGroupesTP(\AppBundle\Entity\Groupe_tp $groupesTP)
    {
        $this->groupesTP[] = $groupesTP;

        return $this;
    }

    /**
     * Remove groupesTP
     *
     * @param \AppBundle\Entity\Groupe_tp $groupesTP
     */
    public function removeGroupesTP(\AppBundle\Entity\Groupe_tp $groupesTP)
    {
        $this->groupesTP->removeElement($groupesTP);
    }

    /**
     * Get groupesTP
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupesTP()
    {
        return $this->groupesTP;
    }

    /**
     * Add diplome
     *
     * @param \AppBundle\Entity\Diplome $diplome
     *
     * @return Departement
     */
    public function addDiplome(\AppBundle\Entity\Diplome $diplome)
    {
        $this->diplomes[] = $diplome;

        return $this;
    }

    /**
     * Remove diplome
     *
     * @param \AppBundle\Entity\Diplome $diplome
     */
    public function removeDiplome(\AppBundle\Entity\Diplome $diplome)
    {
        $this->diplomes->removeElement($diplome);
    }

    /**
     * Get diplomes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiplomes()
    {
        return $this->diplomes;
    }
}
