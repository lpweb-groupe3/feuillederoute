#!/bin/bash
#[INIT]
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\e[33mYellow'
CYAN='\e[36m'
NC='\033[0m'
if ((${EUID:-0} || "$(id -u)")); then
    echo -e "${RED}You are not root."
else
    echo -e "${CYAN}[[[[GET SOURCES FROM GIT]]]]"
    git clone https://gitlab.com/lpweb-groupe3/feuillederoute.git
    cp ./feuillederoute/docker-compose-api.yml.prod ./docker-compose.yml
    touch .env
    #[MYSQL]
    clear
    echo -e "${GREEN}[[[[MYSQL]]]]"
    read -s -p "MYSQL_ROOT_PASSWORD : " MYSQL_ROOT_PASSWORD
    echo "MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}" > ./.env
    echo " "
    read -p "MYSQL_USER : " MYSQL_USER
    echo "MYSQL_USER=${MYSQL_USER}" >> ./.env
    read -s -p "MYSQL_PASSWORD : " MYSQL_PASSWORD
    echo "MYSQL_PASSWORD=${MYSQL_PASSWORD}" >> ./.env
    echo " "
    read -p "MYSQL_DATABASE : " MYSQL_DATABASE
    echo "MYSQL_DATABASE=${MYSQL_DATABASE}" >> ./.env
    #[SYMFONY]
    clear
    echo -e "${YELLOW}[[[[GENERATE SYMFONY CONFIG]]]]"
    echo "parameters: " > ./parameters.yml
    echo "  database_host: db" >> ./parameters.yml
    echo "  database_port: 3306" >> ./parameters.yml
    echo "  database_name: ${MYSQL_DATABASE}" >> ./parameters.yml
    echo "  database_user: ${MYSQL_USER}" >> ./parameters.yml
    echo "  database_password: ${MYSQL_PASSWORD}" >> ./parameters.yml
    echo "  mailer_transport: smtp" >> ./parameters.yml
    echo "  mailer_host: 127.0.0.1" >> ./parameters.yml
    echo "  mailer_user: null " >> ./parameters.yml
    echo "  mailer_password: null " >> ./parameters.yml
    echo "  secret: ThisTokenIsNotSoSecretChangeIt" >> ./parameters.yml
    echo -e "${NC}"
    clear
    cp ./parameters.yml ./feuillederoute/api/app/config/parameters.yml
    #[DOCKER]
    echo -e "${CYAN}[[[[INIT DOCKER]]]]"
    docker-compose up --no-start
    clear
    echo -e "${YELLOW}[[[[INSERT API IN DOCKER]]]]"
    mkdir -p ./data && chmod 755 ./data
    cp -R ./feuillederoute/api ./data/api
    echo -e "${CYAN}[[[[RESTART DOCKER]]]]"
    docker-compose up -d
    #[INTO SYMFONY DOCKER]
    clear
    echo -e "${YELLOW}[[[[UPDATE SYMFONY IN DOCKER]]]]"
    docker exec fdr_symfony bash -c "cd /app/api; \
    composer install; \
    cd ./bin; \
    ./console doctrine:schema:create"
    clear
    echo -e "${CYAN}[[[[RESTART DOCKER AGAIN]]]]"
    docker-compose up -d
    clear
    echo -e "${GREEN}[[[[ALL SEEMS GOOD THE BACK LOOKS FUNCTIONNAL]]]]"
    cat .env
fi        
        
        
    