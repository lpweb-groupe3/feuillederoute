#!/bin/bash
#[INIT]
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\e[33mYellow'
CYAN='\e[36m'
NC='\033[0m'
if ((${EUID:-0} || "$(id -u)")); then
    echo -e "${RED}You are not root."
else
    echo -e "${CYAN}[[[[GET SOURCES FROM GIT]]]]"
    git clone https://gitlab.com/lpweb-groupe3/feuillederoute.git
    cp ./feuillederoute/docker-compose-ng.yml.prod ./docker-compose.yml
    touch .env
    #[SERVER URL]
    clear
    echo -e "${GREEN}[[[[API SERVER URL]]]]"
    echo -e "${NC}"
    clear
    #[DOCKER]
    echo -e "${CYAN}[[[[INIT DOCKER]]]]"
    docker-compose up --no-start
    clear
    echo -e "${YELLOW}[[[[DEPLOY ANGULAR]]]]"
    mkdir -p ./data && chmod 755 ./data
    cp -r ./feuillederoute/ngFeuilleDeRoute/dist/ngFeuilleDeRoute ./data
    echo -e "${CYAN}[[[[RESTART DOCKER]]]]"
    docker-compose up -d
    clear
    echo -e "${GREEN}[[[[ALL SEEMS GOOD THE FRONT LOOKS FUNCTIONNAL]]]]"
    echo -e "${NC}"
fi        
        
        
    