import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Connexion } from '../connexion'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  model = new Connexion();
  //username = model.username;

  getUserName() {
    return this.model.username;

  }

}
