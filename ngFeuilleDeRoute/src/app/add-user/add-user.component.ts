import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { observable } from 'rxjs';
import { Connexion } from '../connexion';
import { Router } from "@angular/router";
import { CookieService } from 'angular2-cookie';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  userList: any = {};

  constructor(private router: Router, private userservice: UserService, private _cookie: CookieService, private userObject: Connexion) { }

  getCookie() {
    console.log(this._cookie);
  }

  setCookie(key, value) {
    this._cookie.put(key, value);
  }

  submitsimple() {
    var connecte = false;
    this.userList.forEach(element => {
      console.log(element);
      if (element.surname == this.model.username) {
        console.log('Connecté : ' + element.surname);
        connecte = true;
      }
    });
    if (connecte) {
      this.setCookie('username', this.model.username);
      this.router.navigate(['/navbar']);
      console.log(this.userObject.username);
    } else {
      this.userObject = {
        'username': this.model.username,
        'password': this.model.password,
        'nom': this.model.nom,
        'prenom': this.model.prenom,
        'mail': this.model.mail,
        'nbHeureMax': this.model.nbHeureMax,
        'nbHeureMin': this.model.nbHeureMin,
        'type': this.model.type,
        'token': this.model.token
      };
      this.setCookie('username', this.model.username);
      this.router.navigate(['/navbar']);
      console.log(this.userObject.username);
    }
  }

  ngOnInit() {

  }

  lUsers = [
        { Name: 'Vacataire', Gender: 'Vacataire' },
        { Name: 'Titulaire', Gender: 'Titulaire' },
        { Name: 'Administrateur', Gender: 'Administrateur'}
      ];

  model = new Connexion();

  submitted = false;

  onSubmit() { this.submitted = true; }

  connect() {
    this.userservice.createUser(
                                this.model.username,
                                this.model.password,
                                this.model.nom,
                                this.model.prenom,
                                this.model.mail,
                                this.model.nbHeureMax,
                                this.model.nbHeureMin,
                                this.model.type
                              ).subscribe((observer) => {
      this.userList = observer;
    });
  }

}
