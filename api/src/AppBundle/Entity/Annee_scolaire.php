<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Annee_scolaire
 *
 * @ORM\Table(name="annee_scolaire")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Annee_scolaireRepository")
 */
class Annee_scolaire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="jourDebut", type="datetime")
     */
    private $jourDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="jourFin", type="datetime")
     */
    private $jourFin;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Semestre::class, cascade={"persist", "remove"}, mappedBy="anneeScolaire")
     */
    protected $semestres;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set jourDebut
     *
     * @param \DateTime $jourDebut
     *
     * @return Annee_scolaire
     */
    public function setJourDebut($jourDebut)
    {
        $this->jourDebut = $jourDebut;

        return $this;
    }

    /**
     * Get jourDebut
     *
     * @return \DateTime
     */
    public function getJourDebut()
    {
        return $this->jourDebut;
    }

    /**
     * Set jourFin
     *
     * @param \DateTime $jourFin
     *
     * @return Annee_scolaire
     */
    public function setJourFin($jourFin)
    {
        $this->jourFin = $jourFin;

        return $this;
    }

    /**
     * Get jourFin
     *
     * @return \DateTime
     */
    public function getJourFin()
    {
        return $this->jourFin;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Annee_scolaire
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->semestres = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add semestre
     *
     * @param \AppBundle\Entity\Semestre $semestre
     *
     * @return Annee_scolaire
     */
    public function addSemestre(\AppBundle\Entity\Semestre $semestre)
    {
        $this->semestres[] = $semestre;

        return $this;
    }

    /**
     * Remove semestre
     *
     * @param \AppBundle\Entity\Semestre $semestre
     */
    public function removeSemestre(\AppBundle\Entity\Semestre $semestre)
    {
        $this->semestres->removeElement($semestre);
    }

    /**
     * Get semestres
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSemestres()
    {
        return $this->semestres;
    }
}
