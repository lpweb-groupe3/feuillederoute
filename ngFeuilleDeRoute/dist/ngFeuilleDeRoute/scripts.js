function openMenu() {
    document.getElementById("sidebar").style.display = "block";
}

function closeMenu() {
    document.getElementById("sidebar").style.display = "none";
}

function sousMenu(id) {
    var element = document.getElementById(id);
    var tabSousMenu = document.getElementsByClassName('sousMenu');
    Array.prototype.forEach.call(tabSousMenu, function (sousMenu) {
        if (sousMenu != element){
            sousMenu.style.display = "none";
        }
    });

    if (element.style.display === "none") {
        element.style.display = "block";
    } else {
        element.style.display = "none";
    }
}

;
//# sourceMappingURL=scripts.js.map