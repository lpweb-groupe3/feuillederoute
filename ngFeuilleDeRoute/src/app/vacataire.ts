export class vacataire {
    nom: string;
    prenom: string;
    mail: string;
    mnemonique: string;
    nbHeureMax: Number;

    constructor(nom: string, prenom: string, mail: string, mnemonique: string, nbHeureMax: Number) {
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.mnemonique = mnemonique;
        this.nbHeureMax = nbHeureMax;
    }
}