<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Association
 *
 * @ORM\Table(name="association")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AssociationRepository")
 */
class Association
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="temp", type="boolean")
     */
    private $temp;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_heures", type="integer")
     */
    private $nbHeures;

    /**
     * @ORM\ManyToMany(targetEntity=Groupe_cm::class)
     */
    protected $groupesCM;

    /**
     * @ORM\ManyToMany(targetEntity=Groupe_td::class)
     */
    protected $groupesTD;

    /**
     * @ORM\ManyToMany(targetEntity=Groupe_tp::class)
     */
    protected $groupesTP;

    /**
     * @ORM\ManyToOne(targetEntity=Enseignement::class, inversedBy="associations")
     */
    protected $enseignement;

    /**
     * @ORM\ManyToOne(targetEntity=Titulaire::class, inversedBy="associations")
     */
    protected $titulaire;

    /**
     * @ORM\ManyToOne(targetEntity=Vacataire::class, inversedBy="associations")
     */
    protected $vacataire;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set temp
     *
     * @param boolean $temp
     *
     * @return Association
     */
    public function setTemp($temp)
    {
        $this->temp = $temp;

        return $this;
    }

    /**
     * Get temp
     *
     * @return bool
     */
    public function getTemp()
    {
        return $this->temp;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->groupesCM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->groupesTD = new \Doctrine\Common\Collections\ArrayCollection();
        $this->groupesTP = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add groupesCM
     *
     * @param \AppBundle\Entity\Groupe_cm $groupesCM
     *
     * @return Association
     */
    public function addGroupesCM(\AppBundle\Entity\Groupe_cm $groupesCM)
    {
        $this->groupesCM[] = $groupesCM;

        return $this;
    }

    /**
     * Remove groupesCM
     *
     * @param \AppBundle\Entity\Groupe_cm $groupesCM
     */
    public function removeGroupesCM(\AppBundle\Entity\Groupe_cm $groupesCM)
    {
        $this->groupesCM->removeElement($groupesCM);
    }

    /**
     * Get groupesCM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupesCM()
    {
        return $this->groupesCM;
    }

    /**
     * Add groupesTD
     *
     * @param \AppBundle\Entity\Groupe_td $groupesTD
     *
     * @return Association
     */
    public function addGroupesTD(\AppBundle\Entity\Groupe_td $groupesTD)
    {
        $this->groupesTD[] = $groupesTD;

        return $this;
    }

    /**
     * Remove groupesTD
     *
     * @param \AppBundle\Entity\Groupe_td $groupesTD
     */
    public function removeGroupesTD(\AppBundle\Entity\Groupe_td $groupesTD)
    {
        $this->groupesTD->removeElement($groupesTD);
    }

    /**
     * Get groupesTD
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupesTD()
    {
        return $this->groupesTD;
    }

    /**
     * Add groupesTP
     *
     * @param \AppBundle\Entity\Groupe_tp $groupesTP
     *
     * @return Association
     */
    public function addGroupesTP(\AppBundle\Entity\Groupe_tp $groupesTP)
    {
        $this->groupesTP[] = $groupesTP;

        return $this;
    }

    /**
     * Remove groupesTP
     *
     * @param \AppBundle\Entity\Groupe_tp $groupesTP
     */
    public function removeGroupesTP(\AppBundle\Entity\Groupe_tp $groupesTP)
    {
        $this->groupesTP->removeElement($groupesTP);
    }

    /**
     * Get groupesTP
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupesTP()
    {
        return $this->groupesTP;
    }

    /**
     * Set enseignement
     *
     * @param \AppBundle\Entity\Enseignement $enseignement
     *
     * @return Association
     */
    public function setEnseignement(\AppBundle\Entity\Enseignement $enseignement = null)
    {
        $this->enseignement = $enseignement;

        return $this;
    }

    /**
     * Get enseignement
     *
     * @return \AppBundle\Entity\Enseignement
     */
    public function getEnseignement()
    {
        return $this->enseignement;
    }

    /**
     * Set titulaire
     *
     * @param \AppBundle\Entity\Titulaire $titulaire
     *
     * @return Association
     */
    public function setTitulaire(\AppBundle\Entity\Titulaire $titulaire = null)
    {
        $this->titulaire = $titulaire;

        return $this;
    }

    /**
     * Get titulaire
     *
     * @return \AppBundle\Entity\Titulaire
     */
    public function getTitulaire()
    {
        return $this->titulaire;
    }

    /**
     * Set vacataire
     *
     * @param \AppBundle\Entity\Vacataire $vacataire
     *
     * @return Association
     */
    public function setVacataire(\AppBundle\Entity\Vacataire $vacataire = null)
    {
        $this->vacataire = $vacataire;

        return $this;
    }

    /**
     * Get vacataire
     *
     * @return \AppBundle\Entity\Vacataire
     */
    public function getVacataire()
    {
        return $this->vacataire;
    }

    /**
     * Set nbHeures
     *
     * @param integer $nbHeures
     *
     * @return Association
     */
    public function setNbHeures($nbHeures)
    {
        $this->nbHeures = $nbHeures;

        return $this;
    }

    /**
     * Get nbHeures
     *
     * @return integer
     */
    public function getNbHeures()
    {
        return $this->nbHeures;
    }
}
