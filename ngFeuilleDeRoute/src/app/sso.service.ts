import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Connexion } from './connexion';


@Injectable({
    providedIn: 'root'
})
export class sso{
  // Définis les routes avec lesquelles nous allons intéragir
  //token 	Token obtenu à l'authentification de l'utilisateur
  //module 	Le nom du module. opt && Valeurs possibles : kawa, candid, covoit, fderoute, altern 1.7
  //url = "https://sso.lpweb-lannion.fr/api/v1.7/check_access/{token}/{module}"

  private url = 'https://sso.lpweb-lannion.fr/api/v1.7/check_access/'

  constructor(private http: HttpClient,private connect: Connexion) { }

  getsso(token) :Observable<Connexion>{
    let url = this.url + token;
    return this.http.get<Connexion>(url);
  }
}
